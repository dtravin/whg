name := "Chess"

version := "1.0"

scalaVersion := "2.13.1"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"


libraryDependencies ++= List( "org.specs2" %% "specs2-core" % "4.8.0" % "test")


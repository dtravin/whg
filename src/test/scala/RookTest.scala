package chess

import Squares._
import Pieces._
import chess.Figures.{Bishop, King, Knight, Pawn, Rook}

class RookTest extends ChessTest {
  "rook" should {
    " in the center of the empty board has 14 possible moves" in {
      val b = WHBoard.withPieces(Map(d4 -> R), White)
      val piece = b.lookupSquare(d4).get.get
      piece.figure shouldEqual Rook
      val validMoves = piece.validMoves(b, d4)
      validMoves shouldEqual List(e4, f4, g4, h4, c4, b4, a4, d5, d6, d7, d8, d3, d2, d1)
    }
  }
  "rook" should {
    " take opponent pieces" in {
      val b = WHBoard.withPieces(Map(a1 -> R, a2 -> Pieces.b, b1 -> Pieces.b), White)
      val piece = b.lookupSquare(a1).get.get
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b1, a2)
    }
  }
  "rook" should {
    " not take own pieces" in {
      val b = WHBoard.withPieces(Map(a1 -> R, a2 -> Pieces.B, c1 -> Pieces.B), White)
      val piece = b.lookupSquare(a1).get.get
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b1)
    }
  }
}

package chess

import org.specs2.mutable.Specification
import Squares._
import Pieces._
import chess.Figures.Pawn

import scala.util.Success

trait ChessTest extends Specification {
  def makeEmptyBoard: Board = WHBoard.empty
  def makeInitalBoard: Board = WHBoard.initial
}

class BoardTest extends ChessTest {
  "empty board" should {
    "look empty" in {
      val ascii ="""
 Move #0
        |
        |
        |
        |
        |
        |
        |
        |
"""
      WHBoard.apply(Map.empty, White).ascii shouldEqual ascii
    }
  }
  "initial board" should {
    "look correctly" in {
val ascii = """
 Move #0
rnbqkbnr|
pppppppp|
        |
        |
        |
        |
PPPPPPPP|
RNBQKBNR|
"""
        WHBoard.initial.ascii shouldEqual ascii
      }
  }
}

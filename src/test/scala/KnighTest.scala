package chess

import Squares._
import Pieces._
import chess.Figures.{Knight, Pawn}

class KnighTest extends ChessTest {
  "knight" should {
    " in the center of the empty board has 8 possible moves" in {
      val b = WHBoard.withPieces(Map(d4 -> N), White)
      val piece = b.lookupSquare(d4).get.get
      piece.figure shouldEqual Knight
      val validMoves = piece.validMoves(b, d4)
      validMoves shouldEqual List(f3, b3, f5, b5, e6, c6, e2, c2)
    }
    " in the corner of empty board has 2 possible moves" in {
      val b = WHBoard.withPieces(Map(a1 -> N), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual Knight
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(c2, b3)
    }
    " cannot eat same color" in {
      val b = WHBoard.withPieces(Map(a1 -> N, c2 -> P, b3 -> Pieces.p), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual Knight
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b3)
    }
  }
}

package chess

import Squares._
import Pieces._
import chess.Figures.{Bishop, King, Knight, Pawn}

class BishopTest extends ChessTest {
  "bishop" should {
    " in the center of the empty board has 13 possible moves" in {
      val b = WHBoard.withPieces(Map(d4 -> B), White)
      val piece = b.lookupSquare(d4).get.get
      piece.figure shouldEqual Bishop
      val validMoves = piece.validMoves(b, d4)
      validMoves shouldEqual List(e3, f2, g1, c3, b2, a1, c5, b6, a7, e5, f6, g7, h8)
    }
  }
  "bishop" should {
    " take opponent pieces" in {
      val b = WHBoard.withPieces(Map(a1 -> B, c3 -> Pieces.b), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual Bishop
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b2, c3)
    }
  }
  "bishop" should {
    " not take own pieces" in {
      val b = WHBoard.withPieces(Map(a1 -> B, c3 -> Pieces.B), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual Bishop
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b2)
    }
  }
}

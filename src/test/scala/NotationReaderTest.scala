package chess

import chess.Figures.Bishop

import scala.util.{Failure, Success}

class NotationReaderTest extends ChessTest {
  "Happy path game" should {
    " be validated with no errors" in {

      val r = new NotationReader("data/checkmate.txt")
      val triedBoard = r.read()
      triedBoard.isSuccess shouldEqual true
    }
  }
  "Invalid game" should {
    " be validated with errors" in {

      val r = new NotationReader("data/sample-moves-invalid.txt")
      val triedBoard = r.read()
      triedBoard.isSuccess shouldEqual false
    }
  }
  "Non existing file" should {
    " read with error" in {

      val r = new NotationReader("data/does-not-exist.txt")
      val triedBoard = r.read()
      triedBoard.isSuccess shouldEqual false
    }
  }
  "Wrong content file" should {
    " read with error" in {

      val r = new NotationReader("data/wrong-content.txt")
      val triedBoard = r.read()
      triedBoard.isSuccess shouldEqual false
    }
  }
}

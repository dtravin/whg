package chess

import Squares._
import Pieces._
import chess.Figures.{Knight, Pawn, King}


class CheckTest extends ChessTest {
  "check" should {
    " be active for white" in {
      val b = WHBoard.withPieces(Map(d5 -> Pieces.k, d4 -> K, a1 -> q), White)
      b.isUnderCheck(White) shouldEqual true
    }

    " no active for white on initial board" in {
      val b = WHBoard.initial
      b.isUnderCheck(White) shouldEqual false
    }

    " no active for black on initial board" in {
      val b = WHBoard.initial
      b.isUnderCheck(Black) shouldEqual false
    }
  }
}

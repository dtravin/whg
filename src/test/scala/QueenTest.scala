package chess

import Squares._
import Pieces._
import chess.Figures.{Bishop, King, Knight, Pawn, Queen, Rook}

class QueenTest extends ChessTest {
  "queen" should {
    " can walk empty diagonal" in {
      val b = WHBoard.withPieces(Map(d1 -> Q, d2 -> P, e1 -> K, c1 -> B, c2 -> P), White)
      val piece = b.lookupSquare(d1).get.get
      piece.figure shouldEqual Queen
      val validMoves = piece.validMoves(b, d1)
      validMoves shouldEqual List(e2, f3, g4, h5)
    }
  }
}

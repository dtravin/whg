package chess

import Squares._
import Pieces._
import chess.Figures.{Knight, Pawn, King}



class KingTest extends ChessTest {
  "king" should {
    " in the center of the empty board has 8 possible moves" in {
      val b = WHBoard.withPieces(Map(d4 -> K), White)
      val piece = b.lookupSquare(d4).get.get
      piece.figure shouldEqual King
      val validMoves = piece.validMoves(b, d4)
      validMoves shouldEqual List(e3, d3, c3, e4, c4, e5, d5, c5)
    }
    " in the corner of empty board has 3 possible moves" in {
      val b = WHBoard.withPieces(Map(a1 -> K), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual King
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b1, b2, a2)
    }
    " cannot eat same color" in {
      val b = WHBoard.withPieces(Map(a1 -> K, b1 -> P, b2 -> Pieces.p), White)
      val piece = b.lookupSquare(a1).get.get
      piece.figure shouldEqual King
      val validMoves = piece.validMoves(b, a1)
      validMoves shouldEqual List(b2, a2)
    }
  }
}

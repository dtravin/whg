package chess

import org.specs2.mutable.Specification
import Squares._
import Pieces._
import chess.Figures.Pawn

import scala.util.Success

class PawnTest extends ChessTest {
  "white pawn " should {
    "be able to move forward direction from e2 to e3 and e4" in {
      val b = WHBoard.initial
      b.lookupSquare(e2).toEither.isLeft shouldEqual false
      val piece = b.lookupSquare(e2).get.get
      piece.figure shouldEqual Pawn
      piece.color shouldEqual White
      val validMoves = piece.validMoves(b, e2)
      validMoves shouldEqual List(e3, e4)
    }
    val b = WHBoard.withPieces(Map(
      e2 -> P, e3 -> Pieces.p,
      h3 -> P,
      d2 -> P, d4 -> Pieces.p,
      b2 -> P, a3 -> Pieces.p, c3 -> Pieces.p, b3 -> Pieces.p,
    ), White)
    b.lookupSquare(e2).toEither.isLeft shouldEqual false
    "cannot eat forward and cannot jump over opponent pawn" in {
      val piece = b.lookupSquare(e2).get.get
      val possibleMoves = piece.validMoves(b, e2)
      possibleMoves.length shouldEqual 0
    }
    "can only go forward one square if moved already" in {
      val piece = b.lookupSquare(h3).get.get
      val validMoves = piece.validMoves(b, h3)
      validMoves shouldEqual List(h4)
    }
    "can only take diagonally a square" in {
      val piece = b.lookupSquare(b2).get.get
      val validMoves = piece.validMoves(b, b2)
      validMoves shouldEqual List(a3, c3)
    }
  }

  "black pawn " should {
    "be able to move forward direction from e7 to e6 and e5" in {
      val b = WHBoard.initialBlackMove
      b.lookupSquare(e7).toEither.isLeft shouldEqual false
      val piece = b.lookupSquare(e7).get.get
      piece.figure shouldEqual Pawn
      piece.color shouldEqual Black
      val validMoves = piece.validMoves(b, e7)
      System.out.println(validMoves)
      validMoves shouldEqual List(e6, e5)
    }

    val b = WHBoard.withPieces(Map(
      e7 -> Pieces.p, e6 -> Pieces.P,
      h6 -> Pieces.p,
      d7 -> Pieces.p, d5 -> Pieces.P,
      b7 -> Pieces.p, a6 -> Pieces.P, c6 -> Pieces.P, b6 -> Pieces.P,
    ), Black)
    b.lookupSquare(e7).toEither.isLeft shouldEqual false
    "cannot eat forward and cannot jump over opponent pawn" in {
      val piece = b.lookupSquare(e7).get.get
      val possibleMoves = piece.validMoves(b, e7)
      possibleMoves.length shouldEqual 0
    }
    "can only go forward one square if moved already" in {
      val piece = b.lookupSquare(h6).get.get
      val validMoves = piece.validMoves(b, h6)
      validMoves shouldEqual List(h5)
    }
    "can only take diagonally a square" in {
      val piece = b.lookupSquare(b7).get.get
      val validMoves = piece.validMoves(b, b7)
      validMoves shouldEqual List(a6, c6)
    }
  }
}

package chess

import scala.util.Success

trait PawnMovement extends Movement {
  override def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = {
    val it = super.possibleMoves(b, sq)
    b.lookupSquare(sq) match {
      case Success(Some(piece)) if b.colorInTurn == piece.color =>
        val mod = if (piece.color == White) -1 else 1
        val doubleStepLine = if (piece.color == White) 6 else 1
        val stepSq = Square(sq.x, sq.y + mod)
        val dirForward = if (sq.y == doubleStepLine) {
          MoveDirection((List(stepSq) ++ List(Square(sq.x, sq.y + 2 * mod))).iterator, false, true)
        } else {
          MoveDirection(List(stepSq).iterator, false, true)
        }
        // Take to the diag left
        val takeDiag1 = Square(sq.x - 1, sq.y + mod)
        val dirTakeDiag1 = b.lookupSquare(takeDiag1) match {
          case Success(Some(opPiece)) if b.colorInTurn != opPiece.color =>
            MoveDirection(List(takeDiag1).iterator, true, false)
          case _ => MoveDirection.empty
        }
        // Take to the diag right
        val takeDiag2 = Square(sq.x + 1, sq.y + mod)
        val dirTakeDiag2 = b.lookupSquare(takeDiag2) match {
          case Success(Some(opPiece)) if b.colorInTurn != opPiece.color =>
            MoveDirection(List(takeDiag2).iterator, true, false)
          case _ => MoveDirection.empty
        }

        it.toList ++ List(dirForward) ++ List(dirTakeDiag1) ++ List(dirTakeDiag2)
      case _ =>
        it
    }
  }
}

trait KingMovement extends Movement {
  override def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = {
    val it = super.possibleMoves(b, sq)
    b.lookupSquare(sq) match {
      case Success(Some(piece)) if b.colorInTurn == piece.color =>
        List(
          Square(sq.x + 1, sq.y + 1),
          Square(sq.x - 0, sq.y + 1),
          Square(sq.x - 1, sq.y + 1),
          Square(sq.x + 1, sq.y - 0),
          Square(sq.x - 1, sq.y - 0),
          Square(sq.x + 1, sq.y - 1),
          Square(sq.x - 0, sq.y - 1),
          Square(sq.x - 1, sq.y - 1),
        ).map(s => {
          b.lookupSquare(s) match {
            case Success(Some(piece)) if piece.color != b.colorInTurn => MoveDirection(List(s).iterator, false, false)
            case Success(None) => MoveDirection(List(s).iterator, false, false)
            case _ => MoveDirection.empty
          }
        }) ++ it.toList
      case _ => it.toList
    }
  }
}

trait LShapeMovement extends Movement {
  override def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = {
    val it = super.possibleMoves(b, sq)
    b.lookupSquare(sq) match {
      case Success(Some(piece)) if b.colorInTurn == piece.color =>
        List(
          Square(sq.x + 2, sq.y + 1),
          Square(sq.x - 2, sq.y + 1),
          Square(sq.x + 2, sq.y - 1),
          Square(sq.x - 2, sq.y - 1),
          Square(sq.x + 1, sq.y - 2),
          Square(sq.x - 1, sq.y - 2),
          Square(sq.x + 1, sq.y + 2),
          Square(sq.x - 1, sq.y + 2),
        ).map(s => {
          b.lookupSquare(s) match {
            case Success(Some(piece)) if piece.color != b.colorInTurn => MoveDirection(List(s).iterator, false, false)
            case Success(None) => MoveDirection(List(s).iterator, false, false)
            case _ => MoveDirection.empty
          }
        }) ++ it.toList
      case _ => it.toList
    }
  }
}

trait LineMovement extends Movement {
  override def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = {
    val it = super.possibleMoves(b, sq)
    b.lookupSquare(sq) match {
      case Success(Some(piece)) if b.colorInTurn == piece.color =>
        List(
          (1, 0),
          (-1, 0),
          (0, -1),
          (0, 1),
        ).map(dir => {
          val squares = (1 to 8).foldLeft(DirectionMoves(List.empty[Square], false)) { (result, delta) =>
            val nextSquare = Square(sq.x + delta * dir._1, sq.y + delta * dir._2)
            if (result.end) result else {
              b.lookupSquare(nextSquare) match {
                case Success(Some(piece)) if piece.color == b.colorInTurn => DirectionMoves(result.squares, true)
                case Success(Some(piece)) if piece.color != b.colorInTurn => DirectionMoves(result.squares ++ List(nextSquare), true)
                case Success(None) => DirectionMoves(result.squares ++ List(nextSquare), false)
                case _ => DirectionMoves(result.squares, true)
              }
            }
          }
          MoveDirection(squares.squares.iterator, false, false)
        }) ++ it.toList
      case _ => it.toList
    }
  }
}

trait DiagonalMovement extends Movement {
  override def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = {
    val it = super.possibleMoves(b, sq)
    b.lookupSquare(sq) match {
      case Success(Some(piece)) if b.colorInTurn == piece.color =>
        List(
          (1, 1),
          (-1, 1),
          (-1, -1),
          (1, -1),
        ).map(dir => {
          val squares = (1 to 8).foldLeft(DirectionMoves(List.empty[Square], false)) { (result, delta) =>
            val nextSquare = Square(sq.x + delta * dir._1, sq.y + delta * dir._2)
            if (result.end) result else {
              b.lookupSquare(nextSquare) match {
                case Success(Some(piece)) if piece.color == b.colorInTurn => DirectionMoves(result.squares, true)
                case Success(Some(piece)) if piece.color != b.colorInTurn => DirectionMoves(result.squares ++ List(nextSquare), true)
                case Success(None) => DirectionMoves(result.squares ++ List(nextSquare), false)
                case _ => DirectionMoves(result.squares, true)
              }
            }
          }
          MoveDirection(squares.squares.iterator, false, false)
        }) ++ it.toList
      case _ => it.toList
    }
  }
}

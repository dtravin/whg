package chess

import scala.collection.immutable.HashSet
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

object Err {
  val InvalidMoveEmptyStartSquare = new Exception("Square is empty")
  val InvalidMoveSquareNotFound = new Exception("Square not found")
}

trait Color {
  def ascii(figure: Figure): Char
  def opponent: Color
}

case class MoveDirection(squares: Iterator[Square], onlyForTaking: Boolean, onlyForEmptySquares: Boolean)
object MoveDirection {
  def empty: MoveDirection = MoveDirection(List.empty[Square].iterator, false, false)
}

trait Movement {
  def possibleMoves(b: Board, sq: Square): Seq[MoveDirection] = List(MoveDirection.empty)
}

trait Figure extends Movement {
  val ascii: Char
  def of(color: Color) = Piece(this, color)
}

case class Square(x: Int, y: Int)

case class DirectionMoves(squares: List[Square], end: Boolean)
case class Piece(figure: Figure, color: Color) {
  def ascii = color.ascii(figure)
  def validMoves(b: Board, sq: Square): List[Square] = figure.possibleMoves(b, sq).map({
    direction =>
      direction.squares.foldLeft(DirectionMoves(List.empty[Square], false)){
      (result, nextSquare) =>
        if (result.end) result else {
          b.lookupSquare(nextSquare) match {
            case Success(Some(piece)) if piece.color == b.colorInTurn => DirectionMoves(result.squares, true)
            case Success(Some(piece)) if piece.color != b.colorInTurn && !direction.onlyForEmptySquares => DirectionMoves(result.squares ++ List(nextSquare), true)
            case Success(None)  if !direction.onlyForTaking => DirectionMoves(result.squares ++ List(nextSquare), false)
            case _ => DirectionMoves(result.squares, true)
          }
        }
    }.squares
  }).flatten.toList
}

trait Board {
  def lookupSquare(sq: Square): Try[Option[Piece]]
  def pieces(color: Color): Map[Square, Piece]
  def ascii: String
  def colorInTurn: Color
  def validate(mr: MoveRequest): Try[ValidMove]
  def applyMove(move: ValidMove): Board
  def isUnderCheck(color: Color): Boolean
}

case class MoveRequest(ax: Int, ay: Int, bx: Int, by: Int)
case class ValidMove(dismissed: List[Square], appeared: List[(Square, Piece)])
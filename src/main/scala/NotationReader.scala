package chess

import com.whitehatgaming.UserInputFile
import scala.util.{Failure, Success, Try}


case class ReaderStep(board: Board, failure: Option[Failure[Any]])

class NotationReader(val fileName: String, val gameObservers: List[Board => Unit] = List.empty) {

  def read():Try[Board] = {
    Try(new UserInputFile(fileName)) match {
      case Failure(exception) =>
        Failure(new Exception("Unexpected error while opening user input file " + exception))
      case Success(fileReader) =>
        LazyList.continually(fileReader.nextMove()).takeWhile(_ != null).
          foldLeft(ReaderStep(WHBoard.initial, None)){ (step, nextMove) =>
            step.failure match {
              case Some(_) => step
              case _ =>
                val mr = MoveRequest(nextMove(0), nextMove(1), nextMove(2), nextMove(3))
                step.board.validate(mr) match {
                  case Success(validMove) =>
                    val boardAfterTurn = step.board.applyMove(validMove)
                    gameObservers.foreach(o => o(boardAfterTurn))
                    ReaderStep(boardAfterTurn, None)
                  case Failure(f) =>
                    ReaderStep(step.board, Some(Failure(f)))
                }
            }
        } match {
          case ReaderStep(_, Some(Failure(f))) => Failure(f)
          case ReaderStep(b, _) => Success(b)
        }
    }
  }

}


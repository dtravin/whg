package chess

import scala.util.{Failure, Success, Try}

case object Black extends Color {
  override def ascii(figure: Figure): Char = figure.ascii.toString.toLowerCase.head
  override def opponent: Color = White
}
case object White extends Color {
  override def ascii(figure: Figure): Char = figure.ascii
  override def opponent: Color = Black
}

object Pieces {
  val P = Piece(figure = Figures.Pawn, color = White)
  val p = Piece(figure = Figures.Pawn, color = Black)
  val K = Piece(figure = Figures.King, color = White)
  val k = Piece(figure = Figures.King, color = Black)
  val Q = Piece(figure = Figures.Queen, color = White)
  val q = Piece(figure = Figures.Queen, color = Black)
  val B = Piece(figure = Figures.Bishop, color = White)
  val b = Piece(figure = Figures.Bishop, color = Black)
  val N = Piece(figure = Figures.Knight, color = White)
  val n = Piece(figure = Figures.Knight, color = Black)
  val R = Piece(figure = Figures.Rook, color = White)
  val r = Piece(figure = Figures.Rook, color = Black)
}

object Figures {
  case object Pawn extends Figure with PawnMovement { val ascii = 'P' }
  case object King extends Figure with KingMovement { val ascii = 'K' }
  case object Knight extends Figure with LShapeMovement { val ascii = 'N' }
  case object Bishop extends Figure with DiagonalMovement { val ascii = 'B' }
  case object Rook extends Figure with LineMovement { val ascii = 'R' }
  case object Queen extends Figure with DiagonalMovement with LineMovement { val ascii = 'Q' }
}

case class WHBoard(squares: Map[Square, Option[Piece]], colorInTurn: Color, totalMoves: Int=0) extends Board {
  override def lookupSquare(sq: Square) =
    squares.get(sq).map(Success(_)) getOrElse {
      Failure(Err.InvalidMoveSquareNotFound)
    }
  override def pieces(color: Color): Map[Square, Piece] = {
    squares.collect({case (k,Some(v)) if v.color == color => (k,v)})
  }

  override def ascii = {
    val a = for (y <- 0 to 7) yield {
      (for (x <- 0 to 7) yield {
        lookupSquare(Square(x, y)) match {
          case Success(Some(piece)) => piece.ascii
          case _ => ' '
        }
      }).mkString + "|\n"
    }
    val checkReport = List(White, Black).map(color => {
      if (isUnderCheck(color)) s"$color CHECK" else ""
    }).mkString
    s"\n Move #$totalMoves\n${a.mkString}$checkReport"
  }

  def verifyMoveIsValid(pieceFrom: Piece, from: Square, to: Square): Try[_] = {
    if (pieceFrom.validMoves(this, from).contains(to)) {
      Success()
    } else {
      Failure(new Exception(s"Cannot move $from to $to"))
    }
  }

  def verifyPlayerNotInCheck(): Try[_] = {
    if (isUnderCheck(color = colorInTurn)) {
      Failure(new Exception("Player cannot end turn on check"))
    } else {
      Success()
    }
  }

  def validate(mr: MoveRequest): Try[ValidMove] = {
    val from  = Square(mr.ax, mr.ay)
    val to  = Square(mr.bx, mr.by)
    for {
      squareFrom <- lookupSquare(from)
      pieceFrom <- squareFrom.map(Success(_)) getOrElse Failure(Err.InvalidMoveEmptyStartSquare)
      squareTo <- lookupSquare(to)
      _ <- verifyMoveIsValid(pieceFrom, from, to)
      _ <- verifyPlayerNotInCheck()
    } yield {
      val dismiss = List(from)
      ValidMove(squareTo.fold(dismiss){ _ => dismiss ++ List(to)}, List((to, pieceFrom)))
    }
  }

  override def applyMove(move: ValidMove): Board = {
    copy(squares=
      squares
        --move.dismissed
        ++move.dismissed.map(e => (e, None))
        ++move.appeared.map(e => (e._1, Some(e._2))),
      colorInTurn=colorInTurn.opponent,
      totalMoves=totalMoves + 1
    )
  }

  override def isUnderCheck(color: Color): Boolean = {
    val maybeSquare = pieces(color).collectFirst({ case (sq, Piece(Figures.King, _)) => sq })
    maybeSquare.fold(false) { kingSquare =>
      val board = copy(colorInTurn=color.opponent)
      board.pieces(color.opponent).iterator.
        map{ case (attackerSquare, attackerPiece) => attackerPiece.validMoves(board, attackerSquare)}.
        flatten.contains(kingSquare)
    }
  }
}


object WHBoard {
  def withPieces(square2pieces: Map[Square, Piece], turn: Color): WHBoard = {
    val squares = (for (y <- 0 to 7) yield {
      for (x <- 0 to 7) yield {
        val sq = Square(x, y)
        square2pieces.get(sq) match {
          case Some(piece) => (sq, Some(piece))
          case _ => (sq, None)
        }
      }
    }).flatten.toMap
    WHBoard(squares, turn)
  }
  def empty: WHBoard = WHBoard.withPieces(Map.empty[Square, Piece], White)
  import Squares._
  import Pieces._

  def initial: WHBoard = WHBoard.withPieces(Map(
    a1 -> R,
    b1 -> N,
    c1 -> B,
    d1 -> Q,
    e1 -> K,
    f1 -> B,
    g1 -> N,
    h1 -> R,
    a8 -> r,
    b8 -> n,
    c8 -> b,
    d8 -> q,
    e8 -> k,
    f8 -> b,
    g8 -> n,
    h8 -> r,
    a2 -> P,
    b2 -> P,
    c2 -> P,
    d2 -> P,
    e2 -> P,
    f2 -> P,
    g2 -> P,
    h2 -> P,
    a7 -> p,
    b7 -> p,
    c7 -> p,
    d7 -> p,
    e7 -> p,
    f7 -> p,
    g7 -> p,
    h7 -> p,
  ), White)

  def initialBlackMove: WHBoard = WHBoard.initial.copy(colorInTurn=Black)
}

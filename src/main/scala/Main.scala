package chess

import scala.util.{Failure, Success, Try}

object Main {

  def main(args: Array[String]) {
    // "data/sample-moves-invalid.txt"
    val r = new NotationReader(args(0), List({ (b: Board) => System.out.println(b.ascii)}))
    r.read() match {
      case Success(board) =>
        System.out.println("Game validated with no errors")
      case Failure(f) =>
        System.out.println(s"Failed to read game with error $f")
    }
  }
}


